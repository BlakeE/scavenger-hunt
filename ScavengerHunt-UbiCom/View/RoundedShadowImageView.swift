//
//  RoundedShadowImageView.swift
//  ScavengerHunt-UbiCom
//
//  Created by Blake Eram on 2019-11-25.
//  Copyright © 2019 BlakeEram. All rights reserved.
//

import UIKit

class RoundedShadowImageView: UIImageView {

    override func awakeFromNib() {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 15
        self.layer.shadowOpacity = 0.75
        self.layer.cornerRadius = 15
    }
    
}
